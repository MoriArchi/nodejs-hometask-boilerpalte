const { UserRepository } = require('../repositories/userRepository');
const { NotFoundException, BadRequestException } = require('../exceptions/restExceptions');

class UserService {

    // TODO: Implement methods to work with user
    create(user) {
        return UserRepository.create(user);
    }

    getUsers() {
        return UserRepository.getAll();
    }

    update(id, data) {
        this.getById(id);
        return UserRepository.update(id, data);
    }

    remove(id) {
        this.getById(id);
        UserRepository.delete(id);
    }

    getById(id) {
        const item = UserRepository.getById(id);
        if (!item) {
            throw new NotFoundException(`No user with id=${id} was found`);
        }
        return item;
    }

    search(data) {
        const item = UserRepository.getOne(data);
        if (!item) {
            throw new BadRequestException(`Credentials are invalid`);
        }
        return item;
    }
}

module.exports = new UserService();