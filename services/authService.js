const UserService = require('./userService');
const { NotAuthorizedException } = require('../exceptions/restExceptions');

class AuthService {
    login(userdata) {
        const user = UserService.search(userdata);
        if (!user) {
            throw new NotAuthorizedException(`Credentials are invalid`);
        }
        return user;
    }
}

module.exports = new AuthService();
