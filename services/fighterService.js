const { FighterRepository } = require('../repositories/fighterRepository');
const { NotFoundException } = require('../exceptions/restExceptions');

class FighterService {
    // TODO: Implement methods to work with fighters
    create(fighter) {
        return FighterRepository.create(fighter);
    }

    getFighters() {
        return FighterRepository.getAll();
    }

    update(id, data) {
        return FighterRepository.update(id, data);
    }

    remove(id) {
        this.getById(id);
        return FighterRepository.delete(id);
    }

    getById(id) {
        const item = FighterRepository.getById(id);
        if (!item) {
            throw new NotFoundException(`No fighter with id=${id} was found`);
        }
        return item;
    }
}

module.exports = new FighterService();
