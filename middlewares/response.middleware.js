const responseMiddleware = ( req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    const { statusCode, data } = res;

    res.status(statusCode).json(data);

    next();
};

exports.responseMiddleware = responseMiddleware;
