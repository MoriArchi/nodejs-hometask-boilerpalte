const { validateUserInput } = require('./helpers/user.validation');
const { getObjectValuesAsString } = require('../services/objectValuesAsString');
const { UserRepository } = require('../repositories/userRepository');
const UserService = require('../services/userService');
const { BadRequestException } = require('../exceptions/restExceptions');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const user = req.body;

    if (UserRepository.getOne({email: user.email})) {
        throw new BadRequestException(`User with email ${user.email} already exists`);
    }
    const { errors, isValid } = validateUserInput(user);

    if (!isValid) {
        throw new BadRequestException(getObjectValuesAsString(errors), errors);
    }

    next();
};

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    // check user existence
    UserService.getById(req.params.id);

    const { errors, isValid } = validateUserInput(req.body);

    if (!isValid) {
        throw new BadRequestException(getObjectValuesAsString(errors), errors);
    }

    next();
};

/**
* Availability of fields
* Field format:
* firstName - notEmpty
* lastName - notEmpty
* email - allow IDN domains and special characters (like Umlauts) before and after the @ sign.
* phoneNumber - + 380xxxxxxxxx
* password - length> = 3
* Id in body of requests should be absent
* Extra fields should not go to the database
*/

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
