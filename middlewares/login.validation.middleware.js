const { validateLoginInput } = require('./helpers/login.validation');
const { getObjectValuesAsString } = require('../services/objectValuesAsString');
const { BadRequestException } = require('../exceptions/restExceptions');

const loginUserValid = (req, res, next) => {
    const { errors, isValid } = validateLoginInput(req.body);

    if (!isValid) {
       throw new BadRequestException(getObjectValuesAsString(errors), errors);
    }

    next();
};

exports.loginUserValid = loginUserValid;
