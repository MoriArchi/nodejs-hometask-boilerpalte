function responseErrorHandlerMiddleware(err, req, res, next) {
    const { statusCode = 400, message, errors } = err;

    console.error("An error occurred:", err);

    res.status(statusCode).json({
        status: statusCode,
        message,
        errors
    });

}

exports.responseErrorHandlerMiddleware = responseErrorHandlerMiddleware;
