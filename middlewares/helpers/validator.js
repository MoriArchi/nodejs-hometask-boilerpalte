class Validator {
    EMAIL_REGEXP = /^[\w.+-_]+@[^.][\w.-]*\.[\w-]{2,63}$/gi;
    PHONE_NUMBER_REGEXP = /^\+380(\d{9})$/g;

    isEmpty = (value) => {
        return value === '' || value === undefined;
    };

    isNumber = (value) => typeof value === 'number' && !Number.isNaN(value);

    isAtLeast = (value, min) => {
        return this.isNumber(value) && value >= min;
    };

    isLessThan = (value, max) => {
        return this.isNumber(value) && value < max;
    };

    isEmail = (value) => {
        return !this.isEmpty(value) && this.EMAIL_REGEXP.test(value);
    };

    isPhoneNumber = (value) => {
        return !this.isEmpty(value) && this.PHONE_NUMBER_REGEXP.test(value);
    };

    containOnlyNecessaryFields = (object, fields) => {
        return  Object.keys(object).every(key => fields.includes(key));
    };

    isMinLength = (value, min) => {
        return !this.isEmpty(value) && value.length >= min;
    };
}

module.exports = new Validator();
