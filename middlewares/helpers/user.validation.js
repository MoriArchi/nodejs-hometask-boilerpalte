const Validator = require('./validator');

const validateUserInput = (data) => {
    let errors = {};

    if (Validator.isEmpty(data.firstName)) {
        errors.firstName = 'First Name is required';
    }

    if (Validator.isEmpty(data.lastName)) {
        errors.lastName = 'Last Name is required';
    }

    if (Validator.isEmpty(data.email)) {
        errors.email = 'Email is required';
    } else if (!Validator.isEmail(data.email)) {
        errors.email = 'Email is invalid';
    }

    if (Validator.isEmpty(data.phoneNumber)) {
        errors.phoneNumber = 'Phone number is required';
    } else if (!Validator.isPhoneNumber(data.phoneNumber)) {
        errors.phoneNumber = 'Phone number should have format: +380xxxxxxxxx';
    }

    if (Validator.isEmpty(data.password)) {
        errors.password = 'Password is required';
    } else if (!Validator.isMinLength(data.password,3)) {
        errors.password = 'Password should be at least 3 characters';
    }

    if (!Validator.containOnlyNecessaryFields(data,['firstName', 'lastName', 'email', 'phoneNumber', 'password'])) {
        errors.fields = 'Should contain only necessary fields: firstName, lastName, email, phoneNumber, password';
    }

    return {
        errors,
        isValid: Object.values(errors).length === 0,
    };
};

exports.validateUserInput = validateUserInput;
