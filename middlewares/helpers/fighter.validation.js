const Validator = require('./validator');

const validateFighterInput = (data) => {
    let errors = {};

    if (Validator.isEmpty(data.name)) {
        errors.name = 'Fighter Name is required';
    }

    if (Validator.isEmpty(data.health)) {
        errors.health = 'Health parameter is required';
    } else if (!Validator.isNumber(data.health)) {
        errors.health = 'Health parameter should be a number';
    } else if (!Validator.isAtLeast(data.health, 1)) {
        errors.health = 'Health parameter should be at least 1';
    }

    if (Validator.isEmpty(data.power)) {
        errors.power = 'Power parameter is required';
    } else if (!Validator.isNumber(data.power)) {
        errors.power = 'Power parameter should be a number';
    } else if (
        !Validator.isAtLeast(data.power, 1) ||
        !Validator.isLessThan(data.power, 101)) {
        errors.power = 'Power parameter should be in range [1, 100]';
    }

    if (Validator.isEmpty(data.defense)) {
        errors.defense = 'Defense parameter is required';
    } else if (!Validator.isNumber(data.defense)) {
        errors.defense = 'Defense parameter should be a number';
    } else if (
        !Validator.isAtLeast(data.defense, 1) ||
        !Validator.isLessThan(data.defense, 11)) {
        errors.defense = 'Defense parameter should be in range [1, 10]';
    }

    if (!Validator.containOnlyNecessaryFields(data, ['name', 'health', 'power', 'defense'])) {
        errors.fields = 'Should contain only necessary fields: name, health, power, defence';
    }

    return {
        errors,
        isValid: Object.values(errors).length === 0,
    };
};

exports.validateFighterInput = validateFighterInput;
