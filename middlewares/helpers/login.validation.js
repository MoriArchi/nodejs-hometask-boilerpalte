const Validator = require('./validator');

const validateLoginInput = (data) => {
    let errors = {};

    if (Validator.isEmpty(data.email)) {
        errors.email = 'Email is required';
    } else if (!Validator.isEmail(data.email)) {
        errors.email = 'Email is invalid';
    }

    if (Validator.isEmpty(data.password)) {
        errors.password = 'Password is required';
    } else if (!Validator.isMinLength(data.password,3)) {
        errors.password = 'Password should be at least 3 characters';
    }

    if (!Validator.containOnlyNecessaryFields(data, ['email', 'password'])) {
        errors.fields = 'Should contain only necessary fields: email, password';
    }

    return {
        errors,
        isValid: Object.values(errors).length === 0,
    };
};

exports.validateLoginInput = validateLoginInput;
