//const { fighter } = require('../models/fighter');
const { validateFighterInput } = require('./helpers/fighter.validation');
const { getObjectValuesAsString } = require('../services/objectValuesAsString');
const FighterService = require('../services/fighterService');
const { BadRequestException } = require('../exceptions/restExceptions');


const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const { errors, isValid } = validateFighterInput(req.body);

    if (!isValid) {
        throw new BadRequestException(getObjectValuesAsString(errors), errors);
    }

    next();
};

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update

    // check fighter existence
    FighterService.getById(req.params.id);

    const { errors, isValid } = validateFighterInput(req.body);

    if (!isValid) {
        throw new BadRequestException(getObjectValuesAsString(errors), errors);
    }

    next();
};

/**
* Availability of fields
* Field format:
* name - not empty
* health - > 1
* power - >= 1 && <100
* defense - >= 1 && <100
* Id in body of requests should be absent
* Extra fields should not go to the database
*/

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
