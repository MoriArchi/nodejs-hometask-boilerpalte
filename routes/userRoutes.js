const { Router } = require('express');
const UserService = require('../services/userService');
const { User } = require('../models/user');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

// @route GET /api/users
// @desc Returns all users in db
router.get('/', (req, res, next) => {
    res.data = UserService.getUsers();

    next();
}, responseMiddleware);

// @route GET /api/users/:id
// @desc Returns specific user by id
router.get('/:id', (req, res, next) => {
    const id = req.params.id;
    res.data = UserService.getById(id);

    next();
}, responseMiddleware);

// @route POST /api/users
// @desc Creates user
router.post('/', createUserValid, (req, res, next) => {
    const user = new User(req.body);
    res.data =  UserService.create(user);

    next();
}, responseMiddleware);

// @route PUT /api/users/:id
// @desc Updates user information details
router.put('/:id', updateUserValid, (req, res, next) => {
    const id = req.params.id;
    const userInfo = req.body;
    res.data = UserService.update(id, userInfo);

    next();

}, responseMiddleware);

// @route DELETE /api/users/:id
// @desc Removes user from db by id
router.delete('/:id', (req, next) => {
    const id = req.params.id;
    UserService.remove(id);

    next();
}, responseMiddleware);

//router.use(responseMiddleware);

module.exports = router;
