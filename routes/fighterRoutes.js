const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { Fighter } = require('../models/fighter');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();


// TODO: Implement route controllers for fighter
// @route GET /api/fighters
// @desc Returns all fighters in db
router.get('/', (req, res, next) => {
    res.data = FighterService.getFighters();

    next();
}, responseMiddleware);

// @route GET /api/fighters/:id
// @desc Returns specific fighter by id
router.get('/:id', (req, res, next) => {
    const id = req.params.id;
    res.data = FighterService.getById(id);

    next();
}, responseMiddleware);

// @route POST /api/fighters
// @desc Creates fighter
router.post('/', createFighterValid, (req, res, next) => {
    const fighter = new Fighter(req.body);
    res.data = FighterService.create(fighter);

    next();
}, responseMiddleware);

// @route PUT /api/fighters/:id
// @desc Changes fighter details by id
router.put('/:id', updateFighterValid, (req, res, next) => {
    const id = req.params.id;
    const fighterInfo = new Fighter(req.body);
    res.data = FighterService.update(id, fighterInfo);

    next();
}, responseMiddleware);

// @route DELETE /api/fighters/:id
// @desc Removes fighter by id
router.delete('/:id', (req, res, next) => {
    const id = req.params.id;
    res.data = FighterService.remove(id);

    next();
}, responseMiddleware);

module.exports = router;
