const { Router } = require('express');
const AuthService = require('../services/authService');
const { loginUserValid } = require('../middlewares/login.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', loginUserValid, (req, res, next) => {
    // TODO: Implement login action (get the user if it exist with entered credentials)
    const email = req.body.email;
    const password = req.body.password;

    res.data = AuthService.login({email, password});

    next();
}, responseMiddleware);

module.exports = router;
