class RestExceptions extends Error {
    constructor(message, statusCode, data) {
        super(message);

        this.statusCode = statusCode;
        this.errors = data;

        Error.captureStackTrace(this, this.constructor);
    }
}

class NotFoundException extends RestExceptions {
    constructor(message, data) {
        super(message, 404, data);
    }
}

class BadRequestException extends RestExceptions {
    constructor(message, data) {
        super(message, 400, data);
    }
}

class NotAuthorizedException extends RestExceptions {
    constructor(message, data) {
        super(message, 401, data);
    }
}

exports.RestException = RestExceptions;
exports.NotFoundException = NotFoundException;
exports.BadRequestException = BadRequestException;
exports.NotAuthorizedException = NotAuthorizedException;