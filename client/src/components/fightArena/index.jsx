import React from 'react';

import {fight} from './fightLogic/fightLogic';
import Modal from '../modal';
import {HealthIndicator, Shield, Punch, Fireball, CritSignal, RageIndicator} from '../createUI';
import Fighter from '../fighter';
import './fightArena.css';

class FightArena extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            winner: null,
        };
    }

    componentDidMount() {
        const {fighter1, fighter2} = this.props;
        fight(fighter1, fighter2).then((fighter) => {
            this.setState({
                winner: fighter,
            });
        });
    }

    render() {
        const {fighter1, fighter2, onCompleteFight} = this.props;

        const {winner} = this.state;

        return (
            <div className="arena___root">
                <Modal winner={winner} onClose={onCompleteFight}/>
                <div className="arena___fight-status">
                    <HealthIndicator fighter={fighter1} position="left"/>
                    <div className="arena___versus-sign">
                        <img
                            src="https://github.com/s1oux/browser-fighter/blob/develop/resources/versus.png?raw=true"
                            alt="vs-sign"
                        />
                    </div>
                    <HealthIndicator fighter={fighter2} position="right"/>
                </div>
                <div className="arena___rage-indicators">
                    <RageIndicator position="left"/>
                    <RageIndicator position="right"/>
                </div>
                <div className="arena___battlefield">
                    <Fighter fighter={fighter1} position="left"/>
                    <Fighter fighter={fighter2} position="right"/>
                </div>
                <div className="arena___shields-container">
                    <Shield position="left"/>
                    <Shield position="right"/>
                </div>
                <div className="arena___punches-container">
                    <Punch position="left"/>
                    <Punch position="right"/>
                </div>
                <div className="arena___fireballs-container">
                    <Fireball position="left"/>
                    <Fireball position="right"/>
                </div>
                <div className="arena___crit-signals-container">
                    <CritSignal position="left"/>
                    <CritSignal position="right"/>
                </div>
            </div>
        );
    }
}

export default FightArena;
