import React from 'react';

import './modal.css';

export default function Modal({ winner, onClose = () => {} }) {
    if (!winner) return null;

    return (
        <div className="modal-layer">
            <div className="modal-root">
                <div className="modal-header">
                    <span>{winner.name} WINS!!!</span>
                    <button className="close-btn" onClick={onClose}>
                        ×
                    </button>
                </div>
                <div className="modal-body">
                    <img
                        className="fighter-preview___img"
                        src={winner.img}
                        alt={winner.name}
                    />
                </div>
            </div>
        </div>
    );
}
