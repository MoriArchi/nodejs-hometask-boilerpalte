import React from 'react';
import { ATTACK_TYPES } from '../../constants/common';

// convert to react component
export function HealthIndicator({fighter, position}) {
    const {name} = fighter;
    const id = `${position}-fighter-indicator`;

    return (
        <div className="arena___fighter-indicator">
            <span className="arena___fighter-name">{name}</span>
            <div className="arena___health-indicator">
                <div id={id} className="arena___health-bar"></div>
            </div>
        </div>
    );
}

export function Shield({position}) {
    const id = `${position}-shield`;
    const positionClassName = `arena___${position}-shield`;

    return (
            <div id={id} className={positionClassName}>
                <img
                    src="../../img/shield.png"
                    alt="shield"
                    className={"shield-img"}>
                </img>
            </div>
    );
}

export function Punch({position}) {
    const positionClassName = `arena___${position}-punch`;
    const id = `${position}-punch`;

    return (
            <div id={id} className={positionClassName}>
                <img src="../../img/punch.png"
                     alt="punch"
                     className={"punch-img"}>
                </img>
            </div>
    );
}

export function Fireball({position}) {
    const id = `${position}-fireball`;
    const positionClassName = `arena___${position}-fireball`;

    return (
            <div id={id} className={positionClassName}>
                <img
                    src="../../img/fireball.gif"
                    alt={ATTACK_TYPES.FIREBALL}
                    className={"fireball-img"}>
                </img>
            </div>
    );
}

export function CritSignal({position}) {
    const id = `${position}-crit-signal`;
    const positionClassName = `arena___${position}-crit-signal`;

    return (
            <div id={id} className={positionClassName}>
                <img
                    src="../../img/fireball.gif"
                    alt={ATTACK_TYPES.FIREBALL}
                    className={"fireball-img"}>
                </img>
            </div>
    );
}

export function RageIndicator({position}) {
    const id = `${position}-rage-indicator`;

    return (
        <div className="arena___rage-indicator">
            <span className="arena___rage-status">Rage</span>
            <div className="arena___fighter-rage">
                <div id={id} className="arena___rage-bar"></div>
            </div>
        </div>
    );
}
